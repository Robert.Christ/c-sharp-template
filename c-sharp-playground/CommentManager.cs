﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using c_sharp_playground.models;

namespace c_sharp_playground
{
    class CommentManager
    {
        public Comment ManipulateComments()
        {

        }

        private List<Comment> GetAllKnownComments()
        {
            var comments = new List<Comment>();
            comments.Add(new Comment("Hi mom! I love the picture!", "Emily"));
            comments.Add(new Comment("Thank you Emily!", "Mom"));
            comments.Add(new Comment("I'm Angry!", "Steve"));
            comments.Add(new Comment("I like that!", "Mark"));
            comments.Add(new Comment("Be sure to like and subscribe!", "Yin"));
            comments.Add(new Comment("Don't be angry Steve!", "Aaron"));

            return comments;
        }
    }
}

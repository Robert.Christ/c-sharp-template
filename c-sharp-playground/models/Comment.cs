﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c_sharp_playground.models
{
    /// <summary>
    /// Represents a comment left by a user on a website.
    /// For example, a comment on an Instagram page, under a picture.
    /// </summary>
    class Comment
    {
        public string Text { get; set; } = null!;

        public string User { get; set; }

        /// <summary>
        /// If any users reply to this comment with comments of their own,
        /// those comments should be added to the Children property.
        /// </summary>
        public List<Comment>? Children { get; set; }

        public Comment(string text, string user)
        {
            Text = text;
            User = user;
            Children = new List<Comment>();
        }
    }
}
